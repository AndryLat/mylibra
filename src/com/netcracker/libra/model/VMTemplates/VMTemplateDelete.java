/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.netcracker.libra.model.VMTemplates;

/**
 * POJO object is to get the name removed from the form template
 * @author andrylat
 */
public class VMTemplateDelete {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
