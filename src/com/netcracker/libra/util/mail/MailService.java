package com.netcracker.libra.util.mail;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 * OLD VERSION! Class that contains methods for working with mail
 *
 * @author andrylat
 */
@Service("mailService")

public class MailService implements IMailService {

    private static JavaMailSender mailSender;
    private static VelocityEngine velocityEngine;
    private static ServletContext servletContext;

    public void setServletContext(ServletContext servletContext) {
        MailService.servletContext = servletContext;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public VelocityEngine getVelocityEngine() {
        return velocityEngine;
    }

    public void setVelocityEngine(VelocityEngine velocityEngine) {
        MailService.velocityEngine = velocityEngine;
    }

    public JavaMailSender getMailSender() {
        return mailSender;
    }

    public void setMailSender(JavaMailSender mailSender) {
        MailService.mailSender = mailSender;
    }

    /**
     * Sends a message to confirm your registration
     *
     * @param adress
     * @param user
     * @param code
     */
    public static void sendConfirmRegistrationMessage(String adress, String user, String code) {
        Map model = new HashMap();
        model.put("index", 0);
        model.put("adress", adress);
        model.put("user", user);
        model.put("code", code);

        MimeMessagePreparator preparator = new message(model);
        mailSender.send(preparator);
    }

    /**
     * Sends a message about successful registration
     *
     * @param adress
     * @param user
     */
    public static void sendSuccessRegistrationMessage(String adress, String user) {
        Map model = new HashMap();
        model.put("index", 1);
        model.put("adress", adress);
        model.put("user", user);

        MimeMessagePreparator preparator = new message(model);
        mailSender.send(preparator);
    }

    /**
     * Sends a letter with a completed application form
     *
     * @param adress
     * @param user
     * @param id
     */
    public static void sendFormMessage(String adress, String user, int id) {
        Map model = new HashMap();
        model.put("index", 2);
        model.put("adress", adress);
        model.put("user", user);
        model.put("id", id);
        MimeMessagePreparator preparator = new messageWithAttachment(model);
        mailSender.send(preparator);
    }

    /**
     * Sends a message to alert the interviewer to conduct the interview
     *
     * @param id
     */
    public static void sendNotificationMessageForInterviewers(int id[]) {
        Map model = new HashMap();
        String adress[] = null;

        model.put("index", 3);
        model.put("adress", adress);
        model.put("date", adress);
        model.put("startTime", adress);
        model.put("finishTime", adress);
        MimeMessagePreparator preparator = new messageForInterviewers(model);
        mailSender.send(preparator);
    }

    public static void sendNoticeMessage(int[] id) {

    }

    /**
     * Returns content template
     *
     * @param index
     * @param model
     * @return
     */
    private static String getTemplateText(int index, Map model) {
        switch (index) {
            case 0:
                return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/template_1.vm", "UTF-8", model);
            case 1:
                return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/template_2.vm", "UTF-8", model);
            case 2:
                return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/template_3.vm", "UTF-8", model);
            case 3:
                return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "/template_4.vm", "UTF-8", model);
            default:
                return null;
        }
    }

    /**
     * Helper class to create a message
     */
    private static class message implements MimeMessagePreparator {

        Map model = null;

        public message(Map model) {
            this.model = model;
        }

        @Override
        public void prepare(MimeMessage mm) throws Exception {

            MimeMessageHelper message = new MimeMessageHelper(mm, "UTF-8");
            message.setTo((String) model.get("adress"));
            message.setText(getTemplateText((int) model.get("index"), model), true);
        }
    }

    /**
     * Helper class to create a message With Attachment
     */
    private static class messageWithAttachment implements MimeMessagePreparator {

        Map model = null;

        public messageWithAttachment(Map model) {
            this.model = model;
        }

        @Override
        public void prepare(MimeMessage mm) throws Exception {
            MimeMessageHelper message = new MimeMessageHelper(mm, true, "UTF-8");
            message.setTo((String) model.get("adress"));
            message.setText(getTemplateText((int) model.get("index"), model), true);
            File file = new File(servletContext.getRealPath("WEB-INF/forms/form" + model.get("id") + ".pdf"));
            message.addAttachment("Your_completed_questionnaire", file);
        }
    }

    /**
     * Helper class to create a message for interviews
     */
    private static class messageForInterviewers implements MimeMessagePreparator {

        Map model = null;

        public messageForInterviewers(Map model) {
            this.model = model;
        }

        @Override
        public void prepare(MimeMessage mm) throws Exception {
            MimeMessageHelper message = new MimeMessageHelper(mm, "UTF-8");
            message.setTo((String[]) model.get("adress"));
            message.setText(getTemplateText((int) model.get("index"), model), true);
        }

    }
}
