--�������� ���� ������������������� ���� ������.
DROP SEQUENCE appRequest_seq; 
DROP SEQUENCE University_seq; 
DROP SEQUENCE Faculty_seq;
DROP SEQUENCE Department_seq;
DROP SEQUENCE User_seq;
DROP SEQUENCE AppForm_seq;
DROP SEQUENCE Advert_seq;
DROP SEQUENCE Lang_seq;
DROP SEQUENCE Roles_seq;
DROP SEQUENCE InterDate_seq;
DROP SEQUENCE Interview_seq;
DROP SEQUENCE Columns_seq;
DROP SEQUENCE Types_seq;
DROP SEQUENCE Topic_seq;
DROP SEQUENCE Template_seq;
DROP SEQUENCE AppLeng_seq;
DROP SEQUENCE ColumnFields_seq;


--�������� ������������������� ���� ������. ������ ������������������ ���������� � 1 � ������ ��� ������������� �� �������. 
 CREATE SEQUENCE appRequest_seq 
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
  CREATE SEQUENCE AppLeng_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

 CREATE SEQUENCE InterDate_seq 
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE Interview_seq 
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
CREATE SEQUENCE Roles_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE University_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

 CREATE SEQUENCE Faculty_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE Department_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE User_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE ColumnFields_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE AppForm_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE Advert_seq 
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE Lang_seq 
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE Template_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE Topic_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE Types_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
 CREATE SEQUENCE Columns_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;
 
/*���������� ��������*/
-- ���������� ����� ������
INSERT INTO Types 
	VALUES(Types_seq.NEXTVAL,'string');
INSERT INTO Types 
	VALUES(Types_seq.NEXTVAL,'integer');		
INSERT INTO Types 
	VALUES(Types_seq.NEXTVAL,'char');	
	
--���������� ���� ��������
INSERT INTO Template 
	VALUES(Template_seq.NEXTVAL, '������� ����� NetCracker ������ �1', 1);
INSERT INTO Template 
	VALUES (Template_seq.NEXTVAL, '������� ����� NetCracker. ������ �2', 0);
	

INSERT INTO Topic 
	VALUES(Topic_seq.NEXTVAL,'��������',NULL,1,NULL, 0);

INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL, 'e-mail', 1, 1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������ ��������', 1, 1);
	
INSERT INTO Topic 
	VALUES(Topic_seq.NEXTVAL,'��������','(����������� ��������� ��������� �������)',1,NULL,0);
	
INSERT INTO Topic 
	VALUES(Topic_seq.NEXTVAL,'��� �������������� (+,�,-,?)', NULL, 1, Topic_seq.CURRVAL-1, 0);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL,'������� �����/����������',1,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL,'������ � �������� NetCracker',1,1);

INSERT INTO Topic 
	VALUES(Topic_seq.NEXTVAL,'������������ ������� ������������',NULL,1,Topic_seq.CURRVAL-2,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL,'���������� ��', 1, 1);
	
INSERT INTO Topic 
	VALUES(Topic_seq.NEXTVAL, '��� ������', NULL, 1, Topic_seq.CURRVAL-3, 1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL, '�������� �������������',1,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������������� ������', 1, 1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '����������� �������������', 1, 1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '�������', 1, 1);
	
INSERT INTO Topic 
	VALUES(Topic_seq.NEXTVAL, '�����������', NULL, 1, NULL, 0);
	
INSERT INTO Topic 
	VALUES(Topic_seq.NEXTVAL,'��� �� ���������� ���� ������ �� ��������(�� ����� �� 0 �� 5)',NULL,1,Topic_seq.CURRVAL-1,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������� ����������', 2, 1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL,'����������� ����������',2,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL,'���',2,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL,'��',2,1 );
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL,'WEB',2,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL,'����������� ��������� (�� Web)',2,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL,'������� ����������������',2,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL,'�������������� ��������',2,1 );
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL-1,'���� � ���� ��� ���� ������ �/��� ���������� ������� ��������, ����� ��',2,1);
	
 INSERT INTO Topic 
	VALUES(Topic_seq.NEXTVAL,'������� ����������� �����',NULL,1,Topic_seq.CURRVAL-2,0);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL,'������',2,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL,'������',2,1 );
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL,'������ ����',2,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL,Topic_seq.CURRVAL-2,'������ ���� ������� ����� � NetCracker(������ ����������, ��������, �������)',1,1);
INSERT INTO Columns 
	VALUES(Columns_seq.NEXTVAL, Topic_seq.CURRVAL-2,'�������������� �������� � ����: ���������, ���������, �����, �����������, ������ ��������, ��.',2,1 );

INSERT INTO Topic VALUES (Topic_seq.NEXTVAL, '��������', NULL, Template_seq.CURRVAL, NULL, 1);

INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, 'E-mail 2', 1, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, 'Skype', 1, 1);

-- ��������
INSERT INTO Topic VALUES (Topic_seq.NEXTVAL, '��������', '(����������� ��������� ��������� ������)', Template_seq.CURRVAL, NULL, 0);
INSERT INTO Topic VALUES (Topic_seq.NEXTVAL, '��� �������������� (+, �, �, ?):', NULL, Template_seq.CURRVAL, Topic_seq.CURRVAL, 0);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������� �����/����������', 1, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������ � �������� NetCracker', 1, 1);

INSERT INTO Topic VALUES (Topic_seq.NEXTVAL, '��� ������:', NULL, Template_seq.CURRVAL, Topic_seq.CURRVAL-1, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '�������� �������������', 1, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������������� ������', 1, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '����������� �������������', 1, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '�������', 1, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '�������� ������ (������ ������������)', 1, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������, ������������� � ����� �����', 1, 1);

-- ����� ���� ���
INSERT INTO Topic VALUES (Topic_seq.NEXTVAL, '����� ���� ���', NULL, Template_seq.CURRVAL, NULL, 0);
INSERT INTO Topic VALUES (Topic_seq.NEXTVAL, '��� �� ���������� ���� ������ �� �������� (�� ����� �� 0 �� 5):', NULL, Template_seq.CURRVAL, Topic_seq.CURRVAL, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������� ����������', 2, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '����������� ���������', 2, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '���', 2, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '���� ������', 2, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '�������������� ��������', 2, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, 'Web', 2, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '���������������� ��� iOS', 2, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '���������������� ��� Android', 2, 1);

INSERT INTO Topic VALUES (Topic_seq.NEXTVAL, '������� ����������� ����� (�� 1 = elementary �� 5 = advanced):', NULL, Template_seq.CURRVAL, Topic_seq.CURRVAL, 0);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������', 2, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������', 2, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '������ ����', 2, 1);

-- ������
INSERT INTO Topic VALUES (Topic_seq.NEXTVAL, '������', NULL, Template_seq.CURRVAL, NULL, 0);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '�������� � ���� ��� ���� ������? �������� ���.', 1, 1);
INSERT INTO Columns VALUES (Columns_seq.NEXTVAL, Topic_seq.CURRVAL, '�������������� �������� � ����: ���������, �����, ����������� � ��.', 1, 1);

	
--���������� � ������� University ��� ����
INSERT INTO UNIVERSITY 
	VALUES(University_seq.NEXTVAL,'��� ��. ���������');
INSERT INTO University   
	VALUES (University_seq.NEXTVAL,'�������� ��������������� �������� ������');
INSERT INTO University 
	VALUES (University_seq.NEXTVAL, '�������� ������������ ��������������� �����������');

--���������� � ������� Faculty �� ��� ���������� ������� ������������ ����
 INSERT INTO FACULTY 
	VALUES(Faculty_seq.NEXTVAL,'���������� ����������', 1);
 INSERT INTO FACULTY 
	VALUES(Faculty_seq.NEXTVAL,'������������ ������� � ����', 1);
 INSERT INTO FACULTY 
	VALUES(Faculty_seq.NEXTVAL,'����������',1);
INSERT INTO Faculty  
	VALUES (Faculty_seq.NEXTVAL,'�������������� ����������',3);
INSERT INTO Faculty   
	VALUES (Faculty_seq.NEXTVAL,'�������� ���������� � ��������',3);
INSERT INTO Faculty  
	VALUES (Faculty_seq.NEXTVAL,'�������� ������������������ ������� � ����������',3);
INSERT INTO Faculty 
	VALUES (Faculty_seq.NEXTVAL, '�������� ������������ ������', 2);
INSERT INTO Faculty 
	VALUES (Faculty_seq.NEXTVAL, '�������� ���������������� � ����������������', 2);
INSERT INTO Faculty 
	VALUES (Faculty_seq.NEXTVAL, '�������� �������, ��������� � �������������� ����������', 2);

--���������� � ������� DEPARTMENT �� ��� ������� � ������� ������������ ����������.
INSERT INTO DEPARTMENT 
	VALUES (Department_seq.NEXTVAL,'��������������� ����������� ������������ ������', 1 );
INSERT INTO DEPARTMENT 
	VALUES (Department_seq.NEXTVAL,'������������ ���������� � ������������� �����������', 1);
INSERT INTO DEPARTMENT 
	VALUES (Department_seq.NEXTVAL,'������� �������������� ������', 1);
INSERT INTO DEPARTMENT 
	VALUES (Department_seq.NEXTVAL,'�������������� ����������',2);
INSERT INTO DEPARTMENT 
	VALUES (Department_seq.NEXTVAL,'������������ ������� � ���������� ����������',2);
INSERT INTO DEPARTMENT 
	VALUES (Department_seq.NEXTVAL, '���',2 );
INSERT INTO DEPARTMENT 
	VALUES (Department_seq.NEXTVAL, '���������������� ���������',3);
INSERT INTO DEPARTMENT 
	VALUES (Department_seq.NEXTVAL, '������ ����������',3);
INSERT INTO DEPARTMENT 
	VALUES (Department_seq.NEXTVAL, '��������������� �������',3);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '�������������� ������', 4);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '������������ ���������������� ������ � �����', 4);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '������������ ������', 4);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '�������������� ������������', 5);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '���������������� ���������', 5);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '����������� ������� � �������������-������������ ����������', 5);	
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '���������� ���������� � �������������� ���������� � �������', 6);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '������������� ����������� � �������������� ����������', 6);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '����������� � ���������� ������� �������������� ������', 6);
INSERT INTO Department  
	VALUES (Department_seq.NEXTVAL, '�������������� ������� � ����',7);
INSERT INTO Department  
	VALUES (Department_seq.NEXTVAL, '�������������-����������� �������',7);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '����������������',7);
INSERT INTO Department 
	VALUES (Department_seq.NEXTVAL, '�������',8);
INSERT INTO Department  
	VALUES (Department_seq.NEXTVAL, '�����- � �����������',8);
INSERT INTO Department  
	VALUES (Department_seq.NEXTVAL, '����������� �������������',8);
INSERT INTO Department  
	VALUES (Department_seq.NEXTVAL, '������',9);
INSERT INTO Department  
	VALUES (Department_seq.NEXTVAL, '����������� �����',9);
INSERT INTO Department  
	VALUES (Department_seq.NEXTVAL, '����������� ���������',9);	
	
--���������� � ������� Advertising 6-�� �������
INSERT INTO Advertising(AdvertisingId,AdvertisingTitle)
    VALUES (Advert_seq.NEXTVAL,'����');
INSERT INTO Advertising(AdvertisingId,AdvertisingTitle)
    VALUES (Advert_seq.NEXTVAL,'�����');
INSERT INTO Advertising(AdvertisingId,AdvertisingTitle)
    VALUES (Advert_seq.NEXTVAL,'����������');
INSERT INTO Advertising(AdvertisingId,AdvertisingTitle)
    VALUES (Advert_seq.NEXTVAL,'������');
INSERT INTO Advertising(AdvertisingId,AdvertisingTitle)
    VALUES (Advert_seq.NEXTVAL,'�������');
INSERT INTO Advertising(AdvertisingId,AdvertisingTitle)
    VALUES (Advert_seq.NEXTVAL,'��������');

--���������� 15 ������ ���������������� � ������� Languages
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'Java');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'C++');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'C#');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'C');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'Pascal');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'PHP');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'LISP');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'Haskell');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'Object Pascal');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'Erlang');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'ML');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'JavaScript');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'Objective-C');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'Prolog');
INSERT INTO Languages 
	VALUES(Lang_seq.NEXTVAL, 'Basic');

--���������� 4 ����� (�������, HR ����������, ����������� ���������� � �������������) � ������� Roles
INSERT INTO Roles(RoleId,Title,AccessLevel)
    VALUES (Roles_seq.NEXTVAL,'Student',0);
INSERT INTO Roles(RoleId,Title,AccessLevel)
    VALUES (Roles_seq.NEXTVAL,'HR Interviewer',1);
INSERT INTO Roles(RoleId,Title,AccessLevel)
    VALUES (Roles_seq.NEXTVAL,'Tech Interviewer',2);
INSERT INTO Roles(RoleId,Title,AccessLevel)
    VALUES (Roles_seq.NEXTVAL,'Administrator',3);
 
--���������� 6-�� ��� ��� �������� � ������� InterviewDate
INSERT INTO InterviewDate 
	VALUES (InterDate_seq.NEXTVAL, TO_DATE('10/12/2012 15:00', 'DD/MM/YYYY hh24:mi'), TO_DATE('10/12/2012 15:20', 'DD/MM/YYYY hh24:mi'), 20);
INSERT INTO InterviewDate 
	VALUES (InterDate_seq.NEXTVAL, TO_DATE('11/12/2012 16:20', 'DD/MM/YYYY hh24:mi'), TO_DATE('11/12/2012 16:40', 'DD/MM/YYYY hh24:mi'), 20);
INSERT INTO InterviewDate 
	VALUES (InterDate_seq.NEXTVAL, TO_DATE('11/12/2012 16:40', 'DD/MM/YYYY hh24:mi'), TO_DATE('11/12/2012 17:00', 'DD/MM/YYYY hh24:mi'), 20);
INSERT INTO InterviewDate 
	VALUES (InterDate_seq.NEXTVAL, TO_DATE('10/05/2013 15:00', 'DD/MM/YYYY hh24:mi'), TO_DATE('10/05/2013 15:20', 'DD/MM/YYYY hh24:mi'), 20);
INSERT INTO InterviewDate 
	VALUES (InterDate_seq.NEXTVAL, TO_DATE('11/05/2013 11:00', 'DD/MM/YYYY hh24:mi'), TO_DATE('11/05/2013 11:20', 'DD/MM/YYYY hh24:mi'), 20);
INSERT INTO InterviewDate 
	VALUES (InterDate_seq.NEXTVAL, TO_DATE('15/05/2013 16:40', 'DD/MM/YYYY hh24:mi'), TO_DATE('15/05/2013 17:00', 'DD/MM/YYYY hh24:mi'), 20);
 
--���������� 72-� ������� � ������� USERS: 1 �������������, 2 HR �����������, 2 ����������� ����������� � 67 C��������
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '�������', '������', 'pupkin.vasiliy@mail.ru',	'1f32aa4c9a1d2ea010adcf2348166a04',	1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '������', '��������', 'kapusta.edik@mail.ru', '1f32aa4c9a1d2ea010adcf2348166a04', 1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '�������', '�����', 'elia.solnce@gmail.com', '1f32aa4c9a1d2ea010adcf2348166a04', 1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '������', '����', 'angel.avrora@gmail.com', '1f32aa4c9a1d2ea010adcf2348166a04', 1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '�������', '������', 'mavrin_dima@gmail.com', '1f32aa4c9a1d2ea010adcf2348166a04', 1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL,'������', '��������', 'leonid_lebedkin@mail.ru', '1f32aa4c9a1d2ea010adcf2348166a04', 1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '�����', '�����', 'janusja.solnyshko@gmail.com', '1f32aa4c9a1d2ea010adcf2348166a04', 1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '����', '�����', 'bojko__ivan@mail.ru', '1f32aa4c9a1d2ea010adcf2348166a04', 1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '�������', '������', 'dimabojchuk@mail.ru', '1f32aa4c9a1d2ea010adcf2348166a04', 1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '�����', '�����','zaharsablja@mail.ru', '1f32aa4c9a1d2ea010adcf2348166a04', 1);
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '�������', '�����','somil_dmitrij@mail.ru', '1f32aa4c9a1d2ea010adcf2348166a04',1 );
INSERT INTO USERS 
	VALUES(User_seq.NEXTVAL, '����', '������','ivanoy_oll@mail.ru', '1f32aa4c9a1d2ea010adcf2348166a04',1 );
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '�������', '�������', 'alexchupahin@gmail.com', 'lad6700', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '������', '������', 'andrepryahin@gmail.com', 'asdfg0987', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '�������', '��������', 'alexshevchenko@gmail.com', 'lkj89gf', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '������', '�����', 'victormal@mail.ru', 'obod67da', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '����', '����������', 'mashatravnikova@gmail.com', 'svob098', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '��������', '�������', 'kristifelman@gmail.com', 'mih5489', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '����', '����', 'russianbanditos@gmail.com', 'kir55po', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '����', '����������', 'semenlitvinenko@gmail.com', 'vid44as', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '�����', '����������', 'vadimpetrovichev@gmail.com', 'odin111', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '�����', '�����', 'denisbelik@gmail.com', 'vlada333', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '�����', '���������', 'mariavershinina@gmail.com', 'pass12345', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '����', '�������', 'yriydubanin@gmail.com', 'gruz200', 4);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL,'������','����','ivanov2013@gmail.com','5000',2);
INSERT INTO Users   
	VALUES (User_seq.NEXTVAL,'�������','������','sidorovich@yahoo.com','1111',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'��������','����','drhouse@ukr.net','12345',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'������','�����','sheldon@mail.ru','1234',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'�������','����������','leo@ukr.net','1247',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'����','���� ������','emma@gmail.com','emma',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'������','����������','radj@yandex.ru','yuliya',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'������','�������','ivanov2@gmail.com','123456789',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'�����','������','penni@urk.net','111111',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'�������','�����','bukin_gen@yahoo.com','5555',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'�����','������','lena_poleno@gmail.com','12345',1);
INSERT INTO Users  
	VALUES (User_seq.NEXTVAL,'���������','�������','sasha@gmail.com','4321',1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '�����', '�����������', 'elena_kon@mail.ru', 'urng67', 2);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '������', '�������', 'andr_mal@yandex.ua', 'nvkr34', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '�������', '������', 'dmtr_obuh@gmail.com', 'ambp53', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '���������', '�������', 'alex_rub@yandex.ua', 'alpr12', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '�����', '��������', 'dar_pust@mail.ru', 'dfku67', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '����', '����������', 'liza_dem@gmail.com', 'poyt67', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '����������', '�������', 'const_kravchuk@mail.ru', 'qwas12', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '�����', '�������', 'villi_demugin@gmail.com', 'zasd34', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '������', '�����������', 'andr_belozrsk@mail.ru', 'qlpo34', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '�����', '����������', 'den_grush@yandex.ua', 'sdfg35', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '���������', '��������', 'ekat_lab@gmail.com', 'tyui64', 1);
INSERT INTO Users 
	VALUES(User_seq.NEXTVAL, '�������', '�����������', 'natali_ogr@yandex.ua', 'lkjh34', 1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'����','�����','sertil@mail.ru','sertil',3);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'������','������','max_cool@ukr.net','monster',1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'�����','���������','alkor@list.ru','alina',1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'�������','���������','nataly@rambler.ru','natashka1995',1);
INSERT INTO Users 
    VALUES (User_seq.NEXTVAL,'�������','�������','lineag_r@yandex.ru','NoStRaDamus',1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'�������','������','applebrend@mail.ru','murzik',1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'�����','�������','pantural@gmail.com','turik114',1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'�������','�������','ocharovashka@mail.ru','thebestgirl',1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'��������','������','koval@rambler.ru','rapislife',1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'���������','�����','whiteangel@gmail.com','angel',1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'�����','�����','vadim_revyk@mail.ru','nastyushka',1);
INSERT INTO Users
    VALUES (User_seq.NEXTVAL,'���������','�������','girla1995@list.ru','tennis471',1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '����', '����������', 'ivanvasilevich90@gmail.com', 'mysuperpass', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '������', '������������', 'light3000@gmail.com', '112233', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '��������', '�����������', 'vinnilikeshoney@gmail.com', 'nicehoney', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '��������', '��������', 'cherkashun@mail.ru', 'drowssap', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '��������', '��������', 'svetka_konfetka@gmail.com', 'oduvanchik', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '�����', '��������', 'alisavstranechudes@gmail.com', 'mih5489', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '����������', '�������', 'dr.freeman333@gmail.com', 'www888', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '����', '������', 'ilya.markin@gmail.com', 'kojo677fs', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '�������', '�����', 'fomin_dmitriy555@gmail.com', 'coca-cola', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '���������', '��������', 'katyakaterina@gmail.com', 'murzik777', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '�������', '���������', 't.kochetkova@gmail.com', 'pass12321word', 1);
INSERT INTO Users 
	VALUES (User_seq.NEXTVAL, '������', '����������', 'maximummax@gmail.com', 'stotri103', 3);
 
--���������� � ������� InterviewerList 12 �������, �.�. ���������� HR � Tech �� ������������� �� ���������� �����.
INSERT INTO InterviewerList 
	VALUES(37, 1);
INSERT INTO InterviewerList 
	VALUES(37, 2);
INSERT INTO InterviewerList 
	VALUES(37, 3);
INSERT INTO InterviewerList 
	VALUES(25, 4);
INSERT INTO InterviewerList 
	VALUES(25, 5);
INSERT INTO InterviewerList 
	VALUES(25, 6);
INSERT INTO InterviewerList 
	VALUES(49, 1);
INSERT INTO InterviewerList 
	VALUES(49, 2);
INSERT INTO InterviewerList 
	VALUES(49, 3);
INSERT INTO InterviewerList 
	VALUES(72, 4);
INSERT INTO InterviewerList 
	VALUES(72, 5);
INSERT INTO InterviewerList 
	VALUES(72, 6);
 
-- �������� ����� ��� 66 ���������
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 1, '��������', '093-643-65-43', 1, 1, NULL, 5, 2013, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'),1, NULL);
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 2, '�������������', '068-657-65-57', 2, 2, NULL, 6, 2011, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 3, '����������', '063-777-65-73', 3, 2, NULL, 5, 2014, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 4, '�������������', '099-767-65-47', 4, 1, NULL, 6, 2010, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);	
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 5, '�������������', '098-777-65-43', 4, 1, NULL, 6, 2010, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);	
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 6, '������������', '063-733-95-00', 5, 6, NULL, 5, 2010, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 7, '�������', '066-773-00-44', 6, 3, NULL, 3, 2016, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);	
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 8, '��������', '066-763-06-10', 8, 1, NULL, 6, 2013, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 9, '������������', '066-763-06-10', 8, 6, NULL, 6, 2013, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);	
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 10, '������������', '068-773-16-15', 4, 3, NULL, 6, 2013, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);
INSERT INTO APPFORM 
	VALUES(AppForm_seq.NEXTVAL, 11, '������������', '0666-88-45-890', 9, 3, NULL, 6, 2013, to_date('21-01-2013 16:38:00','DD-MM-YYYY HH24:MI:SS'), 1, NULL);	
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 13, '������������', '0987692110', 3, 1, NULL, 4, 2013,SYSDATE, 1 , NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 14, '�����������', '380987341818', 7, 3, NULL, 5, 2012, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 15, '��������', '0675414414', 19, 3, NULL, 5, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 16, '�����������', '00380639802325', 24, 1, NULL, 3, 2015, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 17, '��������', '8 096 157 5562', 11, 6, '�������� �������!)', 4, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 18, '������������', '0931518741', 1, 2, NULL, 5, 2010, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 19, '�������������', '0936218976', 7, 6, NULL, 4, 2012, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 20, '�������������', '0978906283', 9, 4, NULL, 5, 2013,SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 21, '���������','0501617845', 23, 2, NULL, 3, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 22, '����������', '0639080541', 5, 1, NULL, 5, 2012, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 23, '��������', '0971718549', 12, 1, NULL, 4, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm
	VALUES (AppForm_seq.NEXTVAL, 26, '�������������', 0935672463, 25, 5, '������ ��� �������', 4, 2014,  SYSDATE, 1, NULL);
INSERT INTO AppForm
	VALUES (AppForm_seq.NEXTVAL, 27, '����������', 0955332463, 22, 1, NULL, 5, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 28, '��������',7587804, 27, 4, '������� ����������', 3, 2015, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 29, '��������', 0971672463, 25, 3, NULL, 4, 2014, SYSDATE, 1, 37 );
INSERT INTO AppForm
	VALUES (AppForm_seq.NEXTVAL, 30, '����������', 0911672465, 22, 5, '������ ��� �������', 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 31, '����������', 0505675569, 21, 3, '�� ������ ����� �������', 2, 2015,SYSDATE, 1, NULL);
INSERT INTO AppForm  
	VALUES (AppForm_seq.NEXTVAL, 32, '��������', 0935672463, 20, 1, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 33, '������������', 0665672411, 23, 1, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm  
	VALUES (AppForm_seq.NEXTVAL, 34, '���������', 0985672333, 22, 3, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm  
	VALUES (AppForm_seq.NEXTVAL, 35, '�����������', 0995672455, 25, 1, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 36, '����������', 0995672455, 24, 1, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 38, '���������', '0671346578', 1, 1,'�������� �����', 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 39, '������������', '0678730690', 2, 2, '�� �����', 3, 2015, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 40, '��������', '0631346679', 3, 1, '� ����', 5, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 41, '��������������', '0661543478', 3, 5, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL,42,	'����������', '80484567821', 10, 4,	'�������� �����', 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 43, '���������', '0631523528',	26,	5, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 44, '����������', '0672353378', 15, 3,	NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 45, '�������������','0665742565', 6, 2, '�������� �����', 3, 2015, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 46, '��������', '0671347456', 17, 2, NULL,	2, 2016, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 47, '����������', '0671346578', 4,	2, NULL, 5,	2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 48, '��������', '0675464565', 8, 1, '�������� �����', 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 50, '�����������', '0935784127', 16, 6, NULL, 3, 2015, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 51, '�����������', '0939784610', 14, 2, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 52, '������������', '0939712654', 11, 3, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 53, '����������', '0938712456', 12, 6, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 54, '������������', '09398741257', 12, 4, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 55, '������������', '0937854321', 12, 4, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 56, '���������', '09394318752', 12, 3, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 57, '��������', '0938731945', 12, 4, NULL,	4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 58, '�������������', '0939831452',	12, 4, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 59, '������������', '0939173258', 12, 1, NULL, 4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 60, '��������', '0936718451', 12, 4, NULL,	4, 2014, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 61, '������������', '0960984687', 25, 2, NULL, 4, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 62, '�����������', '0675419045', 8, 2, NULL, 5, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 63, '��������', '0675414414', 19, 3, NULL, 4, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 64, '�����������', '0504668362', 17, 1, NULL, 3, 2015, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 65, '��������','0674545872', 13, 6, '�������� �������!)', 5, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 66, '���������','0686786733', 1, 2, NULL, 5, 2011, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 67, '�������', '0501557677', 15, 5, NULL, 4, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 68, '����������', '0960026844', 15, 2, NULL, 5, 2013, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 69, '����������', '0638435743', 4, 2, NULL, 4, 2012, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 70, '����������', '0937584977', 6, 1, NULL, 5, 2012, SYSDATE, 1, NULL);
INSERT INTO AppForm 
	VALUES (AppForm_seq.NEXTVAL, 71, '�������������', '0978846721', 9, 2, NULL, 4, 2013, SYSDATE, 1, NULL);

-- ���������� ������ ���������������� � ������ �� ���� ������ ��� ������ ������
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 1, 1, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 1, 2, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 1, 3, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 1, 4, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 2, 1, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 2, 2, 5, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 2, 3, 5, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 2, 4, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 3, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 3, 2, 1, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 3, 10, 5, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 3, 7, 4, 1 );
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 4, 1, 1, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 4, 2, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 4, 6, 5, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 4, 8, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 5, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 5, 2, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 5, 9, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 5, 11, 4, 1);	
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 6, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 6, 2, 1, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 6, 11, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 6, 13, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 7, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 7, 2, 1, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 7, 11, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 7, 13, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 8, 1, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 8, 2, 1, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 8, 14, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 8, 13, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 9, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 9, 2, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 9, 15, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 9, 10, 5, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 10, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 10, 2, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 10, 4, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 10, 5, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 11, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 11, 2, 5, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 11, 5, 5, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 11, 4, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 12, 1, 4, 1 );
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 12, 2, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 12, 7, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 13, 1, 5, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 13, 2, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 13, 8, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 14, 1, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 14, 2, 4, 1 );
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 15, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 15, 2, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 16, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 16, 2, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 17, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 17, 2, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 18, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 18, 2, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 19, 1, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 19, 2, 3 ,1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 20, 1, 5, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 20, 2, 2, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 21, 1, 2 ,1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 21, 2, 4, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 22, 1, 3, 1);
INSERT INTO AppLanguages 
	VALUES (AppLeng_seq.NEXTVAL, 22, 2, 3, 1);	
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 23, 1, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 23, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 23, 10, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 24, 1, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 24, 2, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 24, 13, 1 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 25, 1, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 25, 2, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 25, 11, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 26, 1, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 26, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 26, 13, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 27, 1, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 27, 2, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 27, 11, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 28, 1, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 28, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 28, 11, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 29, 1, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 29, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 29, 6, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 30, 1, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 30, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 30, 9, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 30, 3, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 31, 1, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 31, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 32, 1, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 32, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 32, 3, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 33, 1, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 33, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 33, 3, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 34, 1, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 34, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 34, 4, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 34, 7, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 35, 1, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 35, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 35, 14, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 35, 15, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 36, 1, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 36, 2, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 36, 3, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 36, 4, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 38, 1, 1 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 38, 2, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 38, 6, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 38, 9, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 39, 1, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 39, 2, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 39, 11, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 39, 14, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 40, 1, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 40, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 40, 13, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 40, 3, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 41, 1, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 41, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 41, 7, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 41, 8, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 42, 1, 1 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 42, 2, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 42, 4, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 42, 12, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 43, 1, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 43, 2, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 43, 7, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 43, 13, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 44, 1, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 44, 2, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 44, 5, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 44, 6, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 45, 1, 3 ,1);
INSERT INTO AppLanguages 	
	VALUES(AppLeng_seq.NEXTVAL, 45, 2, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 45, 12, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 45, 13, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 46, 1, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 46, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 47, 1, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 47, 2, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 48, 1, 1 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 48, 2, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 50, 1, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 50, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 50, 1, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 50, 2, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 51, 1, 5 ,1);
INSERT INTO AppLanguages 	
	VALUES(AppLeng_seq.NEXTVAL, 51, 2, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 52, 1, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 52, 2, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 53, 1, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 53, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 54, 1, 1 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 54, 2, 2 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 55, 1, 3 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 55, 2, 4 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 56, 1, 5 ,1);
INSERT INTO AppLanguages 
	VALUES(AppLeng_seq.NEXTVAL, 56, 2, 4 ,1);
 	
--
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 1, 1, 1);
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 2, 2, 1);
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 3, 3, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 4, 4, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 5, 5, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 6, 6, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 1, 7, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 2, 8, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 3, 9, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 4, 10, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 5, 11, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 6, 12, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 1, 13, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 2, 14, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 3, 15, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 4, 16, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 5, 17, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 6, 18, 1);
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 1, 19, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 2, 20, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 3, 21, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 4, 22, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 5, 23, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 6, 24, 1);		
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 1, 25, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 2, 26, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 3, 27, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 4, 28, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 5, 29, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 6, 30, 1);
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 1, 31, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 2, 32, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 3, 33, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 4, 34, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 5, 35, 1);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 6, 36, 1);
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 1, 37, 0);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 2, 38, 0);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 3, 39, 0);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 4, 40, 0);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 5, 41, 0);		
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 6, 65, 1);
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 1, 64, 0);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 2, 63, 0);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 3, 62, 0);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 4, 61, 0);	
INSERT INTO Interview 
	VALUES (Interview_seq.NEXTVAL, 5, 66, 0);


INSERT INTO InterviewResults  
	VALUES (1, 37, 5, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (1, 49, 5, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (2, 37, 4, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (2, 49, 3, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (3, 37, 5, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (3, 49, 3, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (7, 37, 2, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (7, 49, 5, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (8, 37, 4, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (8, 49, 4, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (13, 37, 5, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (13, 49, 5, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (14, 37, 4, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (14, 49, 3, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (15, 49, 5, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (15, 37, 3, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (19, 49, 5, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (19, 37, 3, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (20, 49, 5, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (20, 37, 3, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (21, 49, 4, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (21, 37, 3, '����������� 2');
INSERT INTO InterviewResults  
	VALUES (25, 49, 3, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (25, 37, 3, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (26, 49, 4, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (26, 37, 3, '����������� 2');
INSERT INTO InterviewResults  
	VALUES (27, 49, 4, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (27, 37, 3, '����������� 2');
INSERT INTO InterviewResults  
	VALUES (31, 49, 3, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (31, 37, 3, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (32, 49, 4, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (32, 37, 3, '����������� 2');
INSERT INTO InterviewResults  
	VALUES (37, 49, 1, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (37, 37, 1, '����������� 2');	
INSERT INTO InterviewResults  
	VALUES (38, 49, 2, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (38, 37, 2, '����������� 2');
INSERT INTO InterviewResults  
	VALUES (39, 49, 5, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (39, 37, 5, '����������� 2');
INSERT INTO InterviewResults  
	VALUES (44, 49, 5, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (44, 37, 5, '����������� 2');
INSERT INTO InterviewResults  
	VALUES (43, 49, 5, '����������� 1');	
INSERT INTO InterviewResults  
	VALUES (43, 37, 5, '����������� 2');
	

INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 1, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 1, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 1,'?', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 1, '?', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 1, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 1, '?', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 1, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 1,'4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 1, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 1, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 1,'1', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 1, '0', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 1, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 1,'4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 1,'4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 1, '���� ���� ������ ��������������� � ������������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 1,'4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 1,'4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 1, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 1, '� �����-����� ����!', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,1,2, 'kaputin_eduard@yandexmail', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,2,2, 'Skype: edik.kapustin',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,2, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,2, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,2, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,2, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,2, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,2, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,2, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,2, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,2, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,2, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,2, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,2, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,2, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,2, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,2, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,2, '�� ������ ������ ������� � ����� VariousSoft �� ��������� Junior C++ Developer.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,2, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,2, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 2, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 2, '� ������ ���� ������, � ������� ��������� ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,23,2, '������� �������� ����� � ���������� �� ����������. �������� ����������� � ������� ��������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 3, 'elvira.pp.ua',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,3, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,3, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,3, '+',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,5,'QA',1,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,3, '+',1);		
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 3, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,3, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,3, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,3, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,3, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,3, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,3, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,3, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,3, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,3, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,3, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,3, '0',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,8,'�������� ������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,3, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,3, '������ �����. ����� ����� �� elvira.pp.ua',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,3, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,3, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,21,3, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,3, '� ������� �� ���� �������������. � ���� �������� � ���� �����.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,23,3, '����������� � ������������. ������ ����� � �������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,1,4, 'miss_aleksandrovna@mai.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,2,4, 'vk.com/miss_aleksandrovna',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,4, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,4, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,4, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,4, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,4, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,4, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,4, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,4, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,4, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,4, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,4, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,4, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,4, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,4, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,4, '0',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,8,'OpenGL',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,4, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18, 4, '� ������ ������ �������. ���� ���� ��� ��.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,4, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,4, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,4, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,4, '� ������� �� ���� �������������. � ���� �������� � ���� �����.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,4, '� ������� ���������������� ��������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,5, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,5, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,5, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,5, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,5, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,5, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,5, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,5, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,5, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,5, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,5, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,5, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,5, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,5, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,5, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,5, '� ������ ������ ������� NETSoft.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,5, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,5, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,21,5, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,5, '� ���� �������� ������ ��������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,23,5, '����� ����.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,1,6, 'cat_leopold@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,6, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,6, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,6, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,6, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,6, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,6, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,6, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,6, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,6, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,6, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,6, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,6, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,6, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,6, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,6, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,6, '� ������ ������ ������� ���������� �� �������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,6, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,6, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,21,6, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,6, '� ���� ����������� ���������������� ���������������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,1,7, 'cat_leopold@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,7, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,7, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,7, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,7, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,7, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,7, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,7, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,7, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,7, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,7, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,7, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,7, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,7, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,7, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,7, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,7, '�� �������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,7, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,7, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,21,7, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,7, '� ����� ������������� �������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,23,7, '���� ������ � ����� ����� TOEFL.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,8, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,8, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,8, '-',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,8, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,8, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,8, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,8, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,8, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,8, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,8, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,8, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,8, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,8, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,8, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,8, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18, 8, '�������� ���������� � ������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,8, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,8, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,21,8, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,8, '��������� � �������� ��������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,9, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,9, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,9, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,9, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,9, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,9, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,9, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,9, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,9, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,9, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,9, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,9, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,9, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,9, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,9, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,9, '����� �������� �� ���� ��������� ����',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,9, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,9, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,21,9, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,9, '����� �������������� ���������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,2,10, '�����: SablyaZahar',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,10, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,10, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,10, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,10, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,10, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,10, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,10, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,10, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,10, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,10, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,10, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,10, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,10, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,10, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,10, '1',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,10, '����� ������ ����!',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,10, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,10, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,21,10, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,10, '� ���� ��������� � ��������� ��� �������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,23,10, '���� ������� �� �������� � ����������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,11, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,11, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,11, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,11, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,11, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,11, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,11, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,11, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,11, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,11, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,11, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,11, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,11, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,11, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,11, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,11, '���� ������ �� ������������ ���������������� � ������������ �����.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,11, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,11, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,21,11, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,11, '������ ��� � ������ ��������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,23,11, '���� ������� �� �������� � ����������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,1,12, 'alexchupa@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,2,12, 'alexchupa (skype)',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,12, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,12, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,12, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,12, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,12, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,12, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,12, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,12, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,12, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,12, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,12, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,12, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,12, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,12, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,12, '4',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'���������������������� �������������� �������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL, 12, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18, 12, '����� ������ � ������� IT ���.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,12, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,12, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,12, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,12, '� ����� ����������, ���������� � ���������������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,12, '�������� ������� � �������-IT, ����� 2�� �����.',1);

INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,1,13, 'andrepryah@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,2,13, 'strezz (skype)',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,13, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,13, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,13, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,13, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,13, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,13, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,13, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,13, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,13, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,13, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,13, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,13, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,13, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,13, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,13, '4',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������ ������������ ������ � �������� ����',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL, 13, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,13, '��� 2010 - ������ 2012 part time, junior C++ developer, ATLAS LEGO, ���������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,13, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,13, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,13, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,13, '� ������� �������� � ������, ������� ���������� ��� ���� ������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,13, '��� ������������ � IT-������� ��������� � ������������ �������� ��������� ���������� � 2011 ����.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,2,14, '16956411 icq',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,14, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,14, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,14, '+',1);	
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'�������� �� ������� ��� ��� �������� ������� �������� NetCracker',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,14, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,14, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,14, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,14, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,14, '+',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,5,'project managment',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,14, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,14, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,14, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,14, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,13,14, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,14, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,15,14, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,14, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,14, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,14, '��� ����� ������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,14, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,14, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,14, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,14, '� ���� ��������� ����� ����� �������� ���������� � ���� ����������� � ������ ������ �����, ���� ���� ��� ����� ����� �������� � �� �����������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,14, '�����������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,1,15, 'victormalich@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,2,15, '741-1818 ���. ���.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,3,15, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,4,15, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,5,15, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,6,15, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,7,15, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,8,15, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,9,15, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,10,15, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,11,15, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,12,15, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,15, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,14,15, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,15, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,16,15, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,17,15, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,18,15, '����� � ��������������� ������� ���.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,19,15, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,20,15, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,15, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,22,15, '� ����� ������ � ������, � ������� ����� �������� ����� ����������� ����, � ������, ��� ��� ������ ����� ������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,16, 'http://vk.com/id8947977',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,16, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,16, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,16, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,16, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,16, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,16, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,16, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,16, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,16, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,16, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,16, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,16, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,16, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,16, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,16, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,16, '������������ � ��������� ���������������� ������� "Learn English with pleasure" (���������� Model and View �� Java).',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,16, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,16, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,16, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,16, '������ ��� � ������ ��������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,17, 'kristinafelman@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,17, 'kristi (skype)',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,17, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,17, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,17, '+-',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,17, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,17, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,17, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,17, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,17, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,17, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,17, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,17, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,17, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,17, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,17, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,17, '3',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,3, '0',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,17, '����� �� ����',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,17, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,17, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,17, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,17, '������ ��� ��� ����� ����� ��� ������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,18, 'bandit66@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,18, 'russianbanditos(��� �����)',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,18, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,18, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,18, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,18, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,18, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,18, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,18, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,18, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,18, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,18, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,18, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,18, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,18, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,18, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,18, '4',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'???',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,3, '0',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,18, '��� ����� ������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,18, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,18, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,18, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,18, '���������������, �����������, ���������������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,19, 'litvinensemen@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,19, '80487346768 �������� ���',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,19, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,19, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,19, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,19, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,19, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,19, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,19, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,19, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,19, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,19, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,19, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,19, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,19, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,19, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,19, '4',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,' wtf ',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,3, '0',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,19, '��� ����� :(', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,19, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,19, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,19, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,19, '���� �������, ���� ��������, ���� � NetCracker',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,20, 'huckhuck321@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,20, 'miron321 (�����)',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,20, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,20, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,20, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,20, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,20, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,20, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,20, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,20, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,20, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,20, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,20, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,20, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,20, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,20, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,20, '5',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,3, '0',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,20, '������� ������� �� ������� ���������� � Life', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,20, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,20, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,20, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,20, '���� ������� � ����������� �� ���� ��� ������� � ���������. ������� ������� ������ � ���',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,21, 'deniskaridiska@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,21, 'deniskaridiska (my skype)',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,21, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,21, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,21, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,21, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,21, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,21, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,21, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,21, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,21, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,21, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,21, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,21, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,21, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,21, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,21, '5',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'gsdg',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,3, '0',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,21, '� �������� ��������� ����� ������ � ����� IT �� ����', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,21, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,21, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,21, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,21, '���� �������� �� ���, ��� ������ �� ���������, ���� �� � ��� � ��������. ����� ����� ������� ������� � ����������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,21, '������� �������� ����������� � ������������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,22, 'yriydubanin123@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,22, 'yriydubanin (Skype)',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,22, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,22, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,22, '-',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,22, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,22, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,22, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,22, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,22, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,22, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,22, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,22, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,22, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,22, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,22, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,22, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,22, '������� ���.������� � ������������. ���� ������������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,22, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,22, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,22, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,22, '������� � ��������, ��� ��� �� ������� ��� �������� � ������ ����� ������� ���������...',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,22, '������ ������� � �������-IT 2011 (Java, 1�� �����).',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,23, 'borodach@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,23, 'skype sano',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,23, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,23, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,23, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,23, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,23, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,23, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,23, '+-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������������ ����������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,23, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,23, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,23, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,23, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,23, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,23, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,23, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,23, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,23, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'����',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,23, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,23, '��� �����.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,23, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,23, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,23, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,23, '��� �� �, ������!',1);

INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,24, 'rtrter@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,24, 'ICQ 210141513',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,24, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,24, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,24, '+-',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,24, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,24, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,24, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,24, '-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������������ ��',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,24, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,24, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,24, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,24, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,24, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,24, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,24, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,24, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,24, '4',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'���������� ����������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,24, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,24, '����� �������� ������ �� Java � �������������� ���������������� �� �������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,24, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,24, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,24, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,24, '���� �� �����',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,24, '����� ����������� 2010 ����.',1);

INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,25, 'machoman@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,25, 'vk id 13432',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,25, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,25, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,25, '?',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,25, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,25, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,25, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,25, '-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������������ ��',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,25, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,25, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,25, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,25, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,25, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,25, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,25, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,25, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,25, '4',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������ ������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,25, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,25, '������ ��������� �� PHP � ��������� ��� ���������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,25, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,25, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,25, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,25, '� ����� � ���� �������������� ���������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,25, '����� ����������� 2010 ����.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,26, 'super2009@ukr.net',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,26, 'skype sheldon_don',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,26, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,26, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,26, '?',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,26, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,26, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,26, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,26, '-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������ ������������ ������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,26, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,26, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,26, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,26, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,26, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,26, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,26, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,26, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,26, '4',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'��������� ������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,26, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,26, '���������� ���� �������� ����, ����� ���������� ������� ������������ �������� ����������. � ��� ������� ������ ������ ��������. ��, ������!', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,26, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,26, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,26, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,26, '� ���� ������ ������. ��� ���� �� ���������� �������� ���� �����',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,26, '� ������ ������ ������ � ��������.',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,27, 'leo_leo@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,27, 'facebook id 53453',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,27, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,27, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,27, '+-',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,27, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,27, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,27, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,27, '-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'��������� ���������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,27, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,27, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,27, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,27, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,27, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,27, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,27, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,27, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,27, '4',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'iOS ����������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,27, '4',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,27, '� � �������� �����!', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,27, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,27, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,27, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,27, '������, �������, ������, �������, ����!',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,28, 'sexy-shmeksi@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,28, 'ICQ 210141513',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,28, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,28, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,28, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,28, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,28, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,28, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,28, '-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������������ ��',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,28, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,28, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,28, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,28, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,28, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,28, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,28, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,28, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,28, '5',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������ ������������ ������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,28, '4',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,28, '� �������� � ���-�� ���������� �� ���������, � ����� ����������, �� ��� ���� ��� ������ ������, ��� ������.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,28, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,28, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,28, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,28, '�������� �������, ������ ������ � �����, ������� �� ������������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,29, 'r5435r@yahoo.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,29, 'skype: rara99',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,29, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,29, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,29, '?',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,29, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,29, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,29, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,29, '-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������������ ��',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,29, '+',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,5,'��������� ������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,29, '+',1);		
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,29, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,29, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,29, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,29, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,29, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,29, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,29, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,29, '5',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'���������� ����������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,29, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,29, '��, � �����. ��� ������� ������, ��� ���� �� ������� � ���� �����, �� ���� ��������� � ���������. ��� ������ �� �������� ������ � �������� - � � ��������� ����� � ���� ��������, ������� � � ������� ���������!.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,29, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,29, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,29, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,29, '� ����������, �������� ��!',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,30, 'govard_44@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,30, 'mail.ru govard_ol',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,30, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,30, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,30, '?',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,30, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,30, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,30, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,30, '+',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������ � �������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,30, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,30, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,30, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,30, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,30, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,30, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,30, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,30, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,30, '5',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'��������� ���������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,30, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,30, '����� �������� ������ �� Java � �������������� ���������������� �� �������.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,30, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,30, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,30, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,30, '������ �����',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,30, '�������� ����� � ������� �������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,31, 'penni77@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,31, 'skype penni-penni',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,31, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,31, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,31, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,31, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,31, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,31, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,31, '+-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,31, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,31, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,31, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,31, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,31, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,31, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,31, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,31, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,31, '5',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'����������������� ����������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,31, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,31, '������������ ����� ��������� ������� ���������.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,31, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,31, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,31, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,31, '�� ����� ���������� ���, ��� ����� ������ � ����� ����',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,31, '� ������������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,32, 'gena_shoes@yandex.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,32, 'ICQ 210141513',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,32, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,32, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,32, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,32, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,32, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,32, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,32, '+-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������������� ��������� � �������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,32, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,32, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,32, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,32, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,32, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,32, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,32, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,32, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,32, '5',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������� ����������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,32, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,32, '������������ ���������� �������.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,32, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,32, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,32, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,32, '����� ���������������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,32, '���  �������� �����������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,33, 'lenapoleno@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,33, 'skype poleno',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,33, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,33, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,33, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,33, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,33, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,33, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,33, '+-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������������ ����������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,33, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,33, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,33, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,33, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,33, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,33, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,33, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,33, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,33, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'��������� � ���������� ���������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,33, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,33, '��� �����.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,33, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,33, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,33, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,33, '����� ��������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,34, 'borodach@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,34, 'skype sano',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,34, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,34, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,34, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,34, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,34, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,34, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,34, '+-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������������ ����������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,34, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,34, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,34, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,34, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,34, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,34, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,34, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,34, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,34, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'����',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,34, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,34, '��� �����.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,34, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,34, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,34, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,34, '��� �� �, ������!',1);

INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,35, 'andr_mal222@yandex.ua',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,35, 'ICQ : 26758285',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,35, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,35, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,35, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,35, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,35, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,35, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,35, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,35, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,35, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,35, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,35, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,35, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,35, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,35, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,35, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,35, '����������� �� �++; ���������� ���������� �� C.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,35, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,35, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,35, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,35, '������������, ���������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,35, '����� �������, �� C++',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,36, 'Skype : malinka777',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,36, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,36, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,36, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,36, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,36, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,36, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,36, '+-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'��������� �����������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,36, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,36, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,36, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,36, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,36, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,36, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,36, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,36, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,36, '5',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'Linux',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,36, '4',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,36, '����������� �� Java; ���������� ���������� �� C#', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,36, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,36, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,36, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,36, '����������, ���������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,36, '����� �������, �� C',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,37, '���� : ya_kakashka123.diary.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,37, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,37, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,37, '-',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,37, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,37, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,37, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,37, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,37, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,37, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,37, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,37, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,37, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,37, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,37, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,37, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'3D Max',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,37, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,37, '��������-�������, PHP.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,37, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,37, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,37, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,37, '����������, ��������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,38, 'dar_pust222@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,38, '80483456721',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,38, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,38, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,38, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,38, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,38, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,38, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,38, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,38, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,38, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,38, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,38, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,38, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,38, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,38, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,38, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'Linux',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,38, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,38, '����', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,38, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,38, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,38, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,38, '������������� �������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,39, 'liza_dem222@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,39, 'ICQ : 436363285',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,39, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,39, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,39, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,39, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,39, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,39, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,39, '+-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'��������� �����������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,39, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,39, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,39, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,39, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,39, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,39, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,39, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,39, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,39, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'��������� �����������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,39, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,39, '����������� �� �++; ���������� ���������� �� C.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,39, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,39, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,39, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,39, '������������, ���������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,39, '����� �������, �� Pascal',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,40, 'const_kravchuk222@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,40, 'Skype : costyshka555',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,40, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,40, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,40, '+-',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,40, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,40, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,40, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,40, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,40, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,40, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,40, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,40, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,40, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,40, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,40, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,40, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������ � Photoshop',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,40, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,40, NULL, 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,40, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,40, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,40, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,40, '������������, ����� ���������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,41, 'villi_demugin@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,41, 'ICQ : 549059643',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,41, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,41, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,41, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,41, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,41, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,41, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,41, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,41, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,41, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,41, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,41, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,41, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,41, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,41, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,41, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������ � MS Word',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,41, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,41, '����������� �� �++; ���������� ���������� �� C.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,41, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,41, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,41, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,41, '������������, ���������',1);

INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,42, 'andr_belozrsk222@mail.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,42, 'blog : blabla.blog.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,42, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,42, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,42, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,42, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,42, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,42, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,42, '+-',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'��������� �����������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,42, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,42, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,42, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,42, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,42, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,42, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,42, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,42, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,42, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������������� �����',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,42, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,42, '����������� �� ML; ���������� ���������� �� C.', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,42, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,42, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,42, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,42, '������������, ����� ���������',1);

INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,43, 'den_grush222@yandex.ua',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,43, 'ICQ : 77457455',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,43, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,43, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,43, '+',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,43, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,43, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,43, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,43, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,43, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,43, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,43, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,43, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,43, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,43, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,43, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,43, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������ � Photoshop',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,43, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,43, NULL, 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,43, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,43, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,43, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,43, '������������, ���������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,43, '���.���������, 3-� �����',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,44, 'ekat_lab222@gmail.com',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,44, 'ICQ : 653838735',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,44, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,44, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,44, '?',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'��������� �����',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,44, '+',1);		
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,44, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,44, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,44, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,44, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,44, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,44, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,44, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,44, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,44, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,44, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,44, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,44, '1',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'������ � MS Word',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,44, '5',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,44, '����������� �� �++; ���������� ���������� �� Java', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,44, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,44, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,44, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,44, '������������, ���������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,44, '����� �������, �� C++',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,45, 'natali_ogr222@yandex.ua',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,45, NULL,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,45, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,45, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,45, '?',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,4,'������ � �������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,45, '+',1);		
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,45, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,45, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,45, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,45, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,45, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,45, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,45, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,45, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,45, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,45, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,45, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,45, '5',1);
INSERT INTO Columns VALUES(Columns_seq.NEXTVAL,7,'��������� �����������������',2,1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL,Columns_seq.CURRVAL,45, '4',1);	
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,45, '����������� �� �++; ���������� ���������� �� Java', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,45, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,45, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,45, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,45, '����������, �������������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,46, 'marman@yandex.ua',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,46, 'ICQ : 8713354',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,46, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,46, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,46, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,46, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,46, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,46, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,46, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,46, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,46, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,46, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,46, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,46, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,46, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,46, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,46, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,46, '��� �����', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,46, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,46, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,46, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,46, '����� ����������������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23,46, 'C++',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,47, '1331@yandex.ua',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,47, 'ICQ :8749210',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,47, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,47, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,47, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,47, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,47, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,47, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,47, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,47, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,47, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,47, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,47, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,47, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,47, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,47, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,47, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,47, 'Open 3D Engine (Java)', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,47, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,47, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,47, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,47, '������ ��� ����',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,48, 'marsik@rambler.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,48, 'ICQ : 97412335',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,48, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,48, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,48, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,48, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,48, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,48, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,48, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,48, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,48, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,48, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,48, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,48, '0',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,48, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,48, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,48, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,48, '������ ������ ���������� �� ������������ � ���������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,48, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,48, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,48, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,48, '���� ��������� �� ���� ���',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,49, 'dmitrik_don@rambler.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,49, 'ICQ : 974125',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,49, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,49, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,49, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,49, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,49, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,49, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,49, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,49, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,49, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,49, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,49, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,49, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,49, '1',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,49, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,49, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,49, '�������� �������� �������� �������� �� �++', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,49, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,49, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,49, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,49, '����� ����� �����, ��������, ��������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,50, 'isak@rambler.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,50, 'ICQ : 785620',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,50, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,50, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,50, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,50, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,50, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,50, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,50, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,50, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,50, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,50, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,50, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,50, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,50, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,50, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,50, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,50, '����������� � ���������, ���������, ����������� ����', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,50, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,50, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,50, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,50, 'A�������, ��������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,51, 'turik-murik@rambler.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,51, 'ICQ : 9854120',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,51, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,51, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,51, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,51, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,51, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,51, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,51, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,51, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,51, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,51, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,51, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,51, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,51, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,51, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,51, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,51, '������� �����������, ���� ���� � ���������� ������� ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,51, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,51, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,51, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,51, '�������������, ������� �������� ���������� � ������ �������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,52, 'genka@rambler.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,52, 'ICQ : 789521000',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,52, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,52, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,52, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,52, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,52, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,52, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,52, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,52, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,52, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,52, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,52, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,52, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,52, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,52, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,52, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,52, '������� ����� ���, ����� ��������� (�����, ��������)', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,52, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,52, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,52, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,52, '��������, ����������, ����� ���������� � ���������. ������������ � �����������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,53, 'kovalenkovvv@rambler.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,53, 'ICQ : 12547800',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,53, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,53, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,53, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,53, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,53, '?',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,53, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,53, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,53, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,53, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,53, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,53, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,53, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,53, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,53, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,53, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,53, '������� ����� ���, ����� ��������� (�����, ��������)', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,53, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,53, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,53, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,53, '� ��� ���������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,54, 'belkinkov@rambler.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,54, 'ICQ : 12045785',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,54, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,54, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,54, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,54, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,54, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,54, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,54, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,54, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,54, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,54, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,54, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,54, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,54, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,54, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,54, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,54, '��������� ���� ���������������� �� ����� (����� ���� ���)', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,54, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,54, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,54, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,54, '� ��� ���������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,55, 'revuk@rambler.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,55, 'ICQ : 78954125',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,55, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,55, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,55, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,55, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,55, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,55, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,55, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,55, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,55, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,55, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,55, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,55, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,55, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,55, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,55, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,55, ' ��� �����', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,55, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,55, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,55, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,55, '���� ��������� �� ���� ���, ���� �� ��� ����� ���',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1,56, 'ostashko_nastya@bk.ru',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2,56, 'ICQ : 9546210',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3,56, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4,56, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5,56, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6,56, '+',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7,56, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8,56, '-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9,56, '+-',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10,56, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11,56, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12,56, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13,56, '5',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14,56, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15,56, '3',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16,56, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17,56, '4',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18,56, '����� ����� �� ����, ������ ������ ������� + ������ ����� �� �������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19,56, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20,56, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21,56, '2',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22,56, '���� ����� ������������, ������ �����, ����������� ��� ��������',1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 57, 'buben@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 57, 'buben', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 57, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 57, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 57, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 57, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 57, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 57, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 57, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 57, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 57, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 57, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 57, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 57, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 57, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 57, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 57, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 57, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 57, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 57, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 57, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 57, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 57, '��� ���. ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 58, 'vasiliypozhko@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 58, 'pozhko', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 58, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 58, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 58, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 58, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 58, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 58, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 58, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 58, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 58, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 58, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 58, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 58, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 58, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 58, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 58, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 58, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 58, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 58, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 58, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 58, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 58, '��� ���. ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 59, 'marihka@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 59, 'marichka', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 59, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 59, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 59, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 59, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 59, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 59, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 59, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 59, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 59, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 59, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 59, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 59, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 59, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 59, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 59, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 59, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 59, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 59, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 59, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 59, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 59, '��� ���. ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 60, 'molodoleg90@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 60, 'molodoy', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 60, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 60, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 60, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 60, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 60, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 60, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 60, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 60, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 60, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 60, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 60, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 60, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 60, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 60, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 60, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 60, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 60, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 60, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 60, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 60, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 60, '��� ���. ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 61, 'shectakovadasha@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 61, 'shestakova', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 61, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 61, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 61, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 61, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 61, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 61, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 61, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 61, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 61, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 61, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 61, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 61, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 61, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 61, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 61, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 61, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 61, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 61, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 61, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 61, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 61, '��� ���. ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 62, 'morrzzko@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 62, 'fullhd', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 62, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 62, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 62, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 62, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 62, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 62, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 62, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 62, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 62, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 62, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 62, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 62, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 62, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 62, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 62, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 62, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 62, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 62, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 62, '1', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 62, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 62, '��� ���. ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 63, 'aglishev11@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 63, '---', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 63, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 63, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 63, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 63, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 63, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 63, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 63, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 63, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 63, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 63, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 63, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 63, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 63, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 63, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 63, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 63, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 63, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 63, '?', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 63, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 63, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 63, '��� ���. ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 64, 'steeel@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 64, 'steeelpower', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 64, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 64, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 64, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 64, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 64, '?', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 64, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 64, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 64, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 64, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 64, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 64, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 64, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 64, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 64, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 64, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 64, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 64, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 64, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 64, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 64, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 64, '��� ���. ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 65, 'schets@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 65, 'schets', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 65, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 65, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 65, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 65, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 65, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 65, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 65, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 65, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 65, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 65, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 65, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 65, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 65, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 65, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 65, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 65, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 65, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 65, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 65, '1', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 65, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 65, '��� ���. ��������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 1, 66, 'kuzma@mail.ru', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 2, 66, 'kuzzya', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 3, 66, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 4, 66, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 5, 66, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 6, 66, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 7, 66, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 8, 66, '+-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 9, 66, '-', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 10, 66, '+', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 11, 66, '3', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 12, 66, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 13, 66, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 14, 66, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 15, 66, '1', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 16, 66, '4', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 17, 66, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 18, 66, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 19, 66, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 20, 66, '2', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 21, 66, '5', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 22, 66, '��� ����� ������', 1);
INSERT INTO ColumnFields VALUES (ColumnFields_seq.NEXTVAL, 23, 66, '��� ���. ��������', 1);


INSERT INTO AppRequest 
	VALUES (appRequest_seq.NEXTVAL, 22, '����������', '0639080540', 5, 1, NULL, 5, 2012, '�����', '�����');
INSERT INTO AppRequest 
	VALUES (appRequest_seq.NEXTVAL,42,	'����������', '80484567821', 10, 4,	'�������� �����', 5, 2014,'����','����������');
