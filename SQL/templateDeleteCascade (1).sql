
DROP TABLE NewColumns CASCADE CONSTRAINTS PURGE;
DROP TABLE NewColumns CASCADE CONSTRAINTS PURGE;
Drop   SEQUENCE NewColumns_seq;
 CREATE SEQUENCE NewColumns_seq
 START WITH     1
 INCREMENT BY   1
 NOCACHE
 NOCYCLE;

CREATE TABLE NewColumns (
	ColumnId NUMBER(3) NOT NULL,
	TemplateId NUMBER(4) NOT NULL,
	Name nvarchar2(1000) NOT NULL,
	TypeId NUMBER(2),
	ParentColumn NUMBER(3),
	OrderId number(3),
	CONSTRAINT NewColumnId_pk PRIMARY KEY (ColumnId)
);

CREATE TABLE Types (
	TypeId NUMBER(2) NOT NULL,
	Name varchar2(15) NOT NULL,
	description nvarchar2(1000) not null,
	CONSTRAINT TypeId_pk PRIMARY KEY (TypeId)
);

ALTER TABLE NewColumns DROP CONSTRAINT column_template;
ALTER TABLE NewColumns 
	ADD CONSTRAINT column_template 
	FOREIGN KEY (TemplateId) 
	REFERENCES Template(TemplateId) ON DELETE CASCADE;
	

ALTER TABLE NewColumns DROP CONSTRAINT column_type;
ALTER TABLE NewColumns 
	ADD CONSTRAINT column_type 
	FOREIGN KEY (TypeId) 
	REFERENCES Types(TypeId) ON DELETE CASCADE;


ALTER TABLE ColumnFields DROP CONSTRAINT columF_colum;
ALTER TABLE ColumnFields 
	ADD CONSTRAINT columF_colum 
	FOREIGN KEY (ColumnId) 
	REFERENCES NewColumns(ColumnId)  ON DELETE CASCADE;

ALTER TABLE ColumnFields DROP CONSTRAINT columF_app;
ALTER TABLE ColumnFields 
	ADD CONSTRAINT columF_app 
	FOREIGN KEY (AppId) 
	REFERENCES AppForm(AppId) ON DELETE CASCADE;